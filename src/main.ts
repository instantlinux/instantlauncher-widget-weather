import * as weather from 'yahoo-weather';
import * as path from 'path';
import * as fs from 'fs';

export default function createWidget(widgetfolder: string) {
    var location = "berlin, de";

    if (fs.existsSync(path.join(widgetfolder, "weatherlocation"))) {
        location = fs.readFileSync(path.join(widgetfolder, "weatherlocation")).toString();
    } else { console.error(path.join(widgetfolder, "weatherlocation") + " not found! Defaulting to 'berlin, de'"); }

    // Main Div
    var weatherdiv = document.createElement("div");
    // Styles
    weatherdiv.style.textAlign = "center";
    weatherdiv.style.position = "absolute";
    weatherdiv.style.top = "0";
    weatherdiv.style.left = "0";
    weatherdiv.style.right = "0";
    weatherdiv.style.bottom = "0";
    weatherdiv.style.padding = "20px 0";
    weatherdiv.style.backgroundSize = "cover";

    weather.default(location, "c").then((res: any) => {
        // Temperature text
        var temperaturetext = document.createElement("h3");
        // Style
        temperaturetext.innerHTML = res.item.condition.temp + " °C";
        temperaturetext.style.fontSize = "40px";
        temperaturetext.style.fontFamily = "Raleway";
        temperaturetext.style.fontWeight = "bold";
        temperaturetext.style.letterSpacing = "3px";
        temperaturetext.style.color = "#fafafa";
        temperaturetext.style.margin = "0";
        temperaturetext.style.marginBottom = "23px";
        weatherdiv.appendChild(temperaturetext);

        // Weather description
        var descriptiontext = document.createElement("p");
        // Style
        descriptiontext.innerHTML = res.item.condition.text;
        descriptiontext.style.fontFamily = "Raleway";
        descriptiontext.style.fontSize = "33px";
        descriptiontext.style.fontWeight = "200";
        descriptiontext.style.letterSpacing = "3px";
        descriptiontext.style.margin = "0";
        descriptiontext.style.color = "#fafafa";
        weatherdiv.appendChild(descriptiontext);

        // Weather image
        switch(res.item.condition.code) {
            case "0":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/00.jpg)"; break;
            case "1":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/00.jpg)"; break;
            case "2":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/00.jpg)"; break;
            case "3":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/01.jpg)"; break;
            case "4":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/02.jpg)"; break;
            case "5":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/03.jpg)"; break;
            case "6":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/04.jpg)"; break;
            case "7":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/05.jpg)"; break;
            case "8":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/06.jpg)"; break;
            case "9":  weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/07.jpg)"; break;
            case "10": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/08.jpg)"; break;
            case "11": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/09.jpg)"; break;
            case "12": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/10.jpg)"; break;
            case "13": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/11.jpg)"; break;
            case "14": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/12.jpg)"; break;
            case "15": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/13.jpg)"; break;
            case "16": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/14.jpg)"; break;
            case "17": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/15.jpg)"; break;
            case "18": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/16.jpg)"; break;
            case "19": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/17.jpg)"; break;
            case "20": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/18.jpg)"; break;
            case "21": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/19.jpg)"; break;
            case "22": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/20.jpg)"; break;
            case "23": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/21.jpg)"; break;
            case "24": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/22.jpg)"; break;
            case "25": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/23.jpg)"; break;
            case "26": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/24.jpg)"; break;
            case "27": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/25.jpg)"; break;
            case "28": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/26.jpg)"; break;
            case "29": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/27.jpg)"; break;
            case "30": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/28.jpg)"; break;
            case "31": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/29.jpg)"; break;
            case "32": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/30.jpg)"; break;
            case "33": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/31.jpg)"; break;
            case "34": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/32.jpg)"; break;
            case "35": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/33.jpg)"; break;
            case "36": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/34.jpg)"; break;
            case "37": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/35.jpg)"; break;
            case "38": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/36.jpg)"; break;
            case "39": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/37.jpg)"; break;
            case "40": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/38.jpg)"; break;
            case "41": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/39.jpg)"; break;
            case "42": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/40.jpg)"; break;
            case "43": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/41.jpg)"; break;
            case "44": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/42.jpg)"; break;
            case "45": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/43.jpg)"; break;
            case "46": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/44.jpg)"; break;
            case "47": weatherdiv.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/45.jpg)"; break;
            default: break;
        }
    }).catch((err: any) => { // Called on error
        console.log(err);
        var errortext = document.createElement("p");
        errortext.innerHTML = "Cannot load weather!";
        weatherdiv.appendChild(errortext);
    });

    setTimeout(refreshFunction, 120000, weatherdiv, widgetfolder);

    return weatherdiv;
}

function refreshFunction(element: HTMLElement, widgetfolder: string) {
    console.log("Refreshing weather.");
    var location = "berlin, de";

    if (fs.existsSync(path.join(widgetfolder, "weatherlocation"))) {
        location = fs.readFileSync(path.join(widgetfolder, "weatherlocation")).toString();
    } else { console.error(path.join(widgetfolder, "weatherlocation") + " not found! Defaulting to 'berlin, de'"); }

    weather.default(location, "c").then((res: any) => {
        (element.childNodes[0] as HTMLElement).innerHTML = res.item.condition.temp + " °C";
        (element.childNodes[1] as HTMLElement).innerHTML = res.item.condition.text;
        switch(res.item.condition.code) {
            case "1":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/00.jpg)"; break;
            case "2":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/00.jpg)"; break;
            case "3":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/01.jpg)"; break;
            case "0":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/00.jpg)"; break;
            case "4":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/02.jpg)"; break;
            case "5":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/03.jpg)"; break;
            case "6":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/04.jpg)"; break;
            case "7":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/05.jpg)"; break;
            case "8":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/06.jpg)"; break;
            case "9":  element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/07.jpg)"; break;
            case "10": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/08.jpg)"; break;
            case "11": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/09.jpg)"; break;
            case "12": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/10.jpg)"; break;
            case "13": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/11.jpg)"; break;
            case "14": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/12.jpg)"; break;
            case "15": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/13.jpg)"; break;
            case "16": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/14.jpg)"; break;
            case "17": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/15.jpg)"; break;
            case "18": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/16.jpg)"; break;
            case "19": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/17.jpg)"; break;
            case "20": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/18.jpg)"; break;
            case "21": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/19.jpg)"; break;
            case "22": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/20.jpg)"; break;
            case "23": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/21.jpg)"; break;
            case "24": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/22.jpg)"; break;
            case "25": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/23.jpg)"; break;
            case "26": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/24.jpg)"; break;
            case "27": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/25.jpg)"; break;
            case "28": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/26.jpg)"; break;
            case "29": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/27.jpg)"; break;
            case "30": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/28.jpg)"; break;
            case "31": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/29.jpg)"; break;
            case "32": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/30.jpg)"; break;
            case "33": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/31.jpg)"; break;
            case "34": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/32.jpg)"; break;
            case "35": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/33.jpg)"; break;
            case "36": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/34.jpg)"; break;
            case "37": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/35.jpg)"; break;
            case "38": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/36.jpg)"; break;
            case "39": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/37.jpg)"; break;
            case "40": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/38.jpg)"; break;
            case "41": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/39.jpg)"; break;
            case "42": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/40.jpg)"; break;
            case "43": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/41.jpg)"; break;
            case "44": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/42.jpg)"; break;
            case "45": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/43.jpg)"; break;
            case "46": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/44.jpg)"; break;
            case "47": element.style.backgroundImage = "url(https://guidedlinux.org/weatherimages/45.jpg)"; break;
            default: break;
        }
    }).catch((err: any) => { // Called on error
        console.log(err);
    });

    setTimeout(refreshFunction, 120000, element, widgetfolder);
}