declare module "yahoo-weather" {
    export default function getWeather(
        location: string, 
        unit: string
    ): any;
}